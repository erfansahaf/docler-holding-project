<?php

namespace Tests\Integration;

use App\User;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserAuthenticationTest extends TestCase
{
	use RefreshDatabase;

	/**
	 * @test
	 */
	public function a_user_can_login_with_correct_credentials()
	{
		$user = factory(User::class)->create();
		$isUsernameAndPasswordCorrect = Auth::attempt([
			'email' => $user->email,
			'password' => 'secret'
		]);
		$this->assertTrue($isUsernameAndPasswordCorrect);
	}

	/**
	 * @test
	 */
	public function a_user_can_not_login_with_incorrect_credentials()
	{
		$user = factory(User::class)->create();
		$isUsernameAndPasswordCorrect = Auth::attempt([
			'email' => $user->email,
			'password' => '123456789'
		]);
		$this->assertFalse($isUsernameAndPasswordCorrect);
	}
}
