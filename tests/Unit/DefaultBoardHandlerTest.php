<?php

namespace Tests\Unit;

use App\Http\Handlers\DefaultBoardHandler;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DefaultBoardHandlerTest extends TestCase
{
	private $handler;

	public function setUp()
	{
		$this->handler = new DefaultBoardHandler();
	}

	/**
	 * @test
	 */
	public function it_can_create_an_empty_board()
	{
		$board = $this->handler->getAnEmptyBoard();
		$this->assertCount(9, $board);
	}

	/**
	 * @test
	 */
	public function it_can_set_and_get_board_state()
	{
		$board = ['X', '', '', 'X', '', 'O', 'X', '', 'O'];
		$this->handler->setBoardState($board);
		$currentBoardState = $this->handler->getBoardState();

		$this->assertEquals($board, $currentBoardState);
	}

	/**
	 * @test
	 */
	public function it_can_detect_empty_indexes_and_return_them()
	{
		$board = ['X', '', '', 'X', 'O', 'O', 'X', '', 'O'];
		$this->handler->setBoardState($board);
		$emptyIndexes = $this->handler->getBoardStateEmptyIndexes();
		$this->assertEquals([1, 2, 7], $emptyIndexes);
	}

	/**
	 * @test
	 */
	public function it_can_return_value_of_a_specific_board_state_index()
	{
		$board = ['X', '', '', 'O', '', '', '', '', 'O'];
		$this->handler->setBoardState($board);
		$fourthIndex = $this->handler->getBoardIndex(3);

		$this->assertEquals('O', $fourthIndex);
	}

	/**
	 * @test
	 */
	public function it_can_set_value_to_a_specific_board_state_index()
	{
		$board         = ['X', '', '', 'O', '', '', '', '', 'O'];
		$expectedBoard = ['X', 'X', '', 'O', '', '', '', '', 'O'];
		$this->handler->setBoardState($board);
		$this->handler->setBoardIndex(1, 'X');

		$this->assertEquals($expectedBoard, $this->handler->getBoardState());
	}

	/**
	 * @test
	 */
	public function it_should_detect_current_situation_is_tie()
	{
		$board = ['X', 'O', 'O', 'O', 'X', 'X', 'X', 'O', 'O'];
		$this->handler->setBoardState($board);

		$this->assertTrue($this->handler->isTie());
	}

	/**
	 * @test
	 */
	public function it_should_detect_current_situation_is_not_tie()
	{
		$board = ['X', 'O', '', 'O', 'X', 'X', 'X', 'O', 'O'];
		$this->handler->setBoardState($board);

		$this->assertFalse($this->handler->isTie());
	}

	/**
	 * @test
	 */
	public function it_should_detect_a_player_won_the_game()
	{
		$board = ['X', '', 'O', 'O', 'X', '', '', 'O', 'X'];
		$this->handler->setBoardState($board);

		$this->assertTrue($this->handler->isPlayerWinner('X'));
	}

	/**
	 * @test
	 */
	public function it_should_detect_a_player_did_not_win_the_game()
	{
		$board = ['X', '', 'O', 'O', 'X', '', 'X', 'O', 'O'];
		$this->handler->setBoardState($board);

		$this->assertFalse($this->handler->isPlayerWinner('X'));
	}

	/**
	 * @test
	 */
	public function it_should_detect_an_index_is_taken()
	{
		$board = ['X', '', 'O', 'O', '', '', '', 'X', ''];
		$this->handler->setBoardState($board);

		$this->assertTrue($this->handler->isIndexTaken(7));
	}

	/**
	 * @test
	 */
	public function it_should_detect_an_index_is_not_taken()
	{
		$board = ['X', '', 'O', 'O', '', '', '', 'X', ''];
		$this->handler->setBoardState($board);

		$this->assertNotTrue($this->handler->isIndexTaken(1));
	}

}
