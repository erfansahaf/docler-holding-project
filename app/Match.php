<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
	protected $guarded = ['created_at', 'updated_at'];
	protected $hidden = ['user_id'];

	protected $casts = [
		'board_state' => 'array'
	];
}
