<?php
namespace App\Utilities;


class MatchUtility
{
	public static function getRandomSign()
	{
		$signs = 'XO';
		return $signs[rand(0,1)];
	}

	public static function getOppositeSign($sign)
	{
		if(strtoupper($sign) == 'X')
			return 'O';
		return 'X';
	}

	public static function getRandomTurn()
	{
		$turns = array_values(get_const('match.turn'));
		return $turns[rand(0, count($turns) - 1)];
	}

	public static function getOppositeTurn($turn)
	{
		$turns = get_const('match.turn');
		if($turn == $turns['computer'])
			return $turns['user'];
		return $turns['computer'];
	}

	public static function getRowByIndex($index)
	{
		return (int) $index / 3;
	}

	public static function getColumnByIndex($index)
	{
		return (int) $index % 3;
	}
}