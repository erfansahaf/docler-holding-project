<?php

/*
 * Get CONSTANTS from specified file (config/constants.php)
 */
if(!function_exists('get_const'))
{
	/**
	 * @param string $field
	 * @return mixed
	 */
	function get_const(string $field)
    {
        $sections = explode('.', $field);
        // All CONSTANTS are stored in config/constants.php
        $data = config("constants.".strtoupper(str_singular($sections[0])));
        // If it's
        if(is_array($data) && count($sections) > 1)
        {
        	// Unset called index and iterate over the rest
            unset($sections[0]);
            foreach($sections as $section)
                $data = $data[strtoupper(str_singular($section))];
        }
        return $data;
    }
}

/*
 * It works just like `implode` function, but the KEY is included in output
 */
if(!function_exists('implode_key_value'))
{
	/**
	 * @param string $glue
	 * @param array $pieces
	 *
	 * @return string
	 */
	function implode_key_value(string $glue = "", $pieces)
	{
		return http_build_query($pieces,'', $glue);
	}
}