<?php

namespace App\Http\Logic;


use App\Http\Contracts\AIInterface;
use App\Http\Contracts\BoardHandlerInterface;

class RandomSelection implements AIInterface
{
	/**
	 * This method is responsible to return AI move index on the board state
	 *
	 * @param BoardHandlerInterface $board
	 * @param string $computerSign
	 * @return int
	 */
	public function getMoveIndex(BoardHandlerInterface $board, $computerSign)
	{
		$emptyIndexes = $board->getBoardStateEmptyIndexes();
		return $emptyIndexes[rand(0, count($emptyIndexes) - 1)];
	}
}