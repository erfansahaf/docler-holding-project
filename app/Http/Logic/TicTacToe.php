<?php

namespace App\Http\Logic;


use App\Http\Factories\BoardFactory;

class TicTacToe
{
	private $board;
	private $ai;

	private $turn              = null;

	private $humanPlayer       = null;
	private $computerPlayer    = null;

	public function __construct($boardHandlerDriver, $turn, $humanPlayer, $computerPlayer)
	{
		$this->board = BoardFactory::create($boardHandlerDriver);
		$this->ai    = new RandomSelection(); // It can be Minimax also

		$this->turn = $turn;
		$this->humanPlayer    = $humanPlayer;
		$this->computerPlayer = $computerPlayer;
	}

	public function newGame()
	{
		$this->board->setBoardState($this->board->getAnEmptyBoard());

		return $this;
	}

	public function resumeGame($boardState)
	{
		$this->board->setBoardState($boardState);

		return $this;
	}

	public function getBoardState()
	{
		return $this->board->getBoardState();
	}

	public function getTurn()
	{
		return $this->turn;
	}

	public function toggleTurn()
	{
		$humanPlayerTurnValue  = get_const('match.turn.user');
		$computerPlayerTurnValue = get_const('match.turn.computer');
		if($this->turn == $computerPlayerTurnValue)
			$this->turn = $humanPlayerTurnValue;
		else
			$this->turn = $computerPlayerTurnValue;

		return $this;
	}

	public function setTurn($turn)
	{
		$this->turn = $turn;
		return $this;
	}

	public function getHumanPlayer()
	{
		return $this->humanPlayer;
	}

	public function setHumanPlayer($humanPlayer)
	{
		$this->humanPlayer = $humanPlayer;
		return $this;
	}

	public function humanPlays($index)
	{
		$this->checkIsIndexInRange($index);

		if($this->board->isIndexTaken($index))
			return $this;

		$this->board->setBoardIndex($index, $this->humanPlayer);

		$this->toggleTurn();

		return $this;
	}

	public function getComputerPlayer()
	{
		return $this->computerPlayer;
	}

	public function setComputerPlayer($computerPlayer)
	{
		$this->computerPlayer = $computerPlayer;
		return $this;
	}

	/**
	 * @return integer
	 */
	public function computerPlays()
	{
		$index = $this->ai->getMoveIndex($this->board, $this->computerPlayer);
		$this->checkIsIndexInRange($index);

		if($this->board->isIndexTaken($index))
			return null;

		$this->board->setBoardIndex($index, $this->computerPlayer);

		$this->toggleTurn();

		return $index;
	}

	public function getAvailableIndexes()
	{
		return $this->board->getBoardStateEmptyIndexes();
	}

	public function isUserTurn()
	{
		return ( $this->turn == get_const('match.turn.user') );
	}

	public function isComputerTurn()
	{
		return ( $this->turn == get_const('match.turn.computer') );
	}

	public function isTie()
	{
		return $this->board->isTie();
	}

	public function isPlayerWinner($player)
	{
		return $this->board->isPlayerWinner($player);
	}

	public function getStatus()
	{
		if($this->board->isPlayerWinner($this->humanPlayer))
			return get_const('match.status.user_won');
		elseif($this->board->isPlayerWinner($this->computerPlayer))
			return get_const('match.status.computer_won');
		elseif($this->board->isTie())
			return get_const('match.status.draw');
		else
			return get_const('match.status.not_finished');
	}

	private function checkIsIndexInRange($index)
	{
		if($index < 0 || $index > 8)
			throw new \Exception("Provided Index is out of range. Should be between 0 and 8.");
	}
}