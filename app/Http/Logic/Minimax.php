<?php
/**
 * Created by PhpStorm.
 * User: Erfan
 * Date: 7/6/2018
 * Time: 2:27 AM
 */

namespace App\Http\Logic;


use App\Http\Contracts\AIInterface;
use App\Http\Contracts\BoardHandlerInterface;

class Minimax implements AIInterface
{
	/**
	 * This method is responsible to return AI move index on the board state
	 *
	 * @param BoardHandlerInterface $board
	 * @param string $computerSign
	 * @return int
	 */
	public function getMoveIndex(BoardHandlerInterface $board, $computerSign)
	{
		// Didn't have enough time to implement this algorithm
		// Use RandomSelection implementation instead
		// My Apology! :(

		// TODO: Implement getMoveIndex() method.
	}
}