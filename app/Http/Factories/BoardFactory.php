<?php

namespace App\Http\Factories;


use App\Http\Handlers\DefaultBoardHandler;
use App\Http\Handlers\DoclerHoldingBoardHandler;

class BoardFactory
{
	public static function create($handler)
	{
		$handlerInstance = null;

		switch ($handler){
			case 'docler':
				$handlerInstance = new DoclerHoldingBoardHandler();
				break;
			case 'default':
				$handlerInstance = new DefaultBoardHandler();
				break;
			default:
				throw new \Exception("Unsupported BoardHandler provided.");
				break;
		}

		return $handlerInstance;
	}
}