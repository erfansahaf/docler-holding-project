<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Contracts\MoveInterface;
use App\Http\Logic\TicTacToe;
use App\Match;
use App\User;
use App\Utilities\MatchUtility;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class MatchesController extends Controller implements MoveInterface
{
	public function create()
	{
		$boardHandler = env('BOARD_HANDLER', 'default');
		$randomTurn = MatchUtility::getRandomTurn();
		$userPlayerSign = MatchUtility::getRandomSign();
		$computerPlayerSign = MatchUtility::getOppositeSign($userPlayerSign);

		$game = new TicTacToe($boardHandler, $randomTurn, $userPlayerSign, $computerPlayerSign);
		$game->newGame();

		if($game->isComputerTurn())
			$game->computerPlays();

		$data = [
			'user_id'       => Auth::user()->id,
			'user_sign'     => $game->getHumanPlayer(),
			'turn'          => $game->getTurn(),
			'board_state'   => $game->getBoardState(),
			'board_handler' => $boardHandler,
		];
		$match = Match::create($data);

		return response([
			'status' => 201,
			'message' => 'New match created successfully.',
			'data' => $match
		], 201);
	}

	public function move(Request $request, $id)
	{
		$match = Match::where('id', $id)
			->where('user_id', $request->user()->id)
			->whereIn('status', [
				get_const('match.status.created'),
				get_const('match.status.not_finished')
			])
			->first();
		if(!$match)
			return response([
				'status' => 404,
				'message' => 'No open game found.'
			], 404);

		$game = new TicTacToe($match->board_handler, $match->turn, $match->user_sign, MatchUtility::getOppositeSign($match->user_sign));
		$game = $game->resumeGame($match->board_state);

		$availableIndexes = $game->getAvailableIndexes();

		$this->validate($request, [
			'index' => ['required', 'integer', Rule::in($availableIndexes)]
		]);

		if($game->isUserTurn()){
			$isFinished = false;

			$game->humanPlays($request->get('index'));
			if($game->isPlayerWinner($game->getHumanPlayer())){
				$isFinished = true;
				$message = "User won the game.";
			}
			elseif ($game->isTie()){
				$isFinished = true;
				$message = 'It\'s Tie';
			}
			else {
				$computerCheckedIndex = $game->computerPlays();
				if($game->isPlayerWinner($game->getComputerPlayer())){
					$isFinished = true;
					$message = 'Computer played and won the game.';
				}
				elseif ($game->isTie()){
					$isFinished = true;
					$message = 'It\'s Tie';
				}
				else {
					$message = 'Move saved successfully and computer played as well.';
				}
			}

			$match->turn = $game->getTurn();
			$match->board_state = $game->getBoardState();
			$match->status = $game->getStatus();
			$match->save();

			$data = collect($match)->put('is_finished', $isFinished);
			if(isset($computerCheckedIndex))
				$data->put('computer_move_index', $computerCheckedIndex);

			return response([
				'status' => 200,
				'message' => $message,
				'data' => $data
			], 200);
		}

		return response([
			'status' => 406,
			'message' => 'It\'s not user turn.',
			'data'   => null,
		], 406);
	}

	public function makeMove($boardState, $playerUnit = 'X')
	{
		$boardHandler = 'docler';
		$computerPlayerSign = MatchUtility::getOppositeSign($playerUnit);
		$turn = get_const('match.turn.user');

		$game = new TicTacToe($boardHandler, $turn, $playerUnit, $computerPlayerSign);
		$index = $game->resumeGame($boardState)->computerPlays();

		return [
			MatchUtility::getColumnByIndex($index),
			MatchUtility::getRowByIndex($index),
			$computerPlayerSign
		];
	}
}
