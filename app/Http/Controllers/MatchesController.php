<?php

namespace App\Http\Controllers;

use App\Match;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MatchesController extends Controller
{
	public function index()
	{
		// Get last 10 matches
		$matches = Match::where('user_id', Auth::id())->latest()->take(10)->get();
		return view('matches', ['matches' => $matches]);
	}

	public function new()
	{
		return view('game-board');
	}
}
