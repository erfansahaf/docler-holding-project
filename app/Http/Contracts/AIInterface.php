<?php

namespace App\Http\Contracts;


interface AIInterface
{

	/**
	 * This method is responsible to return AI move index on the board state
	 *
	 * @param BoardHandlerInterface $board
	 * @param string $computerSign
	 * @return int
	 */
	public function getMoveIndex(BoardHandlerInterface $board, $computerSign);
}