<?php

namespace App\Http\Contracts;


interface BoardHandlerInterface
{
	/**
	 *  Get current board state
	 *
	 * @return array
	 */
	public function getBoardState();

	/**
	 * Set board state
	 *
	 * @param array $boardState
	 * @return BoardHandlerInterface
	 */
	public function setBoardState($boardState);

	/**
	 * Getting default game board
	 *
	 * @return array
	 */
	public function getAnEmptyBoard();

	/**
	 * Get those indexes which have not taken yet by any player
	 *
	 * @return array
	 */
	public function getBoardStateEmptyIndexes();

	/**
	 * Get value of a specific board index
	 *
	 * @param integer $index
	 * @return string
	 */
	public function getBoardIndex($index);

	/**
	 * Set value to a specific board index
	 *
	 * @param $index
	 * @param $player
	 * @return BoardHandlerInterface
	 */
	public function setBoardIndex($index, $player);

	/**
	 * Checking the game state if it's Tie (Draw)
	 *
	 * @return boolean
	 */
	public function isTie();

	/**
	 * Checking if the Player is winner
	 *
	 * @param string $player
	 * @return boolean
	 */
	public function isPlayerWinner($player);

	/**
	 * Check is board index is taken before or not
	 *
	 * @param integer $index
	 * @return boolean
	 */
	public function isIndexTaken($index);
}