<?php

namespace App\Http\Handlers;

use App\Http\Contracts\BoardHandlerInterface;

class DoclerHoldingBoardHandler implements BoardHandlerInterface
{
	private $boardState = [];
	private $winningConditions = [
		[[0, 0], [0, 1], [0, 2]],
		[[1, 0], [1, 1], [1, 2]],
		[[2, 0], [2, 1], [2, 2]],

		[[0, 0], [1, 0], [2, 0]],
		[[0, 1], [1, 1], [2, 1]],
		[[0, 2], [1, 2], [2, 2]],

		[[0, 0], [1, 1], [2, 2]],
		[[0, 2], [1, 1], [2, 0]]
	];

	/**
	 *  Get current board state
	 *
	 * @return array
	 */
	public function getBoardState()
	{
		return $this->boardState;
	}

	/**
	 * Set board state
	 *
	 * @param array $boardState
	 * @return BoardHandlerInterface
	 */
	public function setBoardState($boardState)
	{
		$this->boardState = $boardState;
		return $this;
	}

	/**
	 * Getting default game board
	 *
	 * @return array
	 */
	public function getAnEmptyBoard()
	{
		return [
			['', '', ''],
			['', '', ''],
			['', '', '']
		];
	}

	/**
	 * Get those indexes which have not taken yet by any player

	 * @return array
	 */
	public function getBoardStateEmptyIndexes()
	{
		$emptyIndexes = [];
		foreach($this->boardState as $rowKey => $row){
			foreach($row as $columnKey => $column){
				if($column == '')
					$emptyIndexes[] = $this->getBoardPositionIndex($rowKey, $columnKey);
			}
		}

		return $emptyIndexes;
	}

	/**
	 * Get value of a specific board index
	 *
	 * @param integer $index
	 * @return string
	 */
	public function getBoardIndex($index)
	{
		$row = $this->getRowByIndex($index);
		$column = $this->getColumnByIndex($index);

		return $this->boardState[$row][$column];
	}

	/**
	 * Set value to a specific board index
	 *
	 * @param $index
	 * @param $player
	 * @return BoardHandlerInterface
	 */
	public function setBoardIndex($index, $player)
	{
		$row = $this->getRowByIndex($index);
		$column = $this->getColumnByIndex($index);

		$this->boardState[$row][$column] = $player;
		return $this;
	}

	/**
	 * Checking the game state if it's Tie (Draw)
	 *
	 * @return boolean
	 */
	public function isTie()
	{
		$isBoardStateFilledOut =  count($this->getBoardStateEmptyIndexes()) < 1 ;

		return $isBoardStateFilledOut;
	}

	/**
	 * Checking if the Player is winner
	 *
	 * @param string $player
	 * @return bool
	 */
	public function isPlayerWinner($player)
	{
		foreach($this->winningConditions as $condition){
			if(
				$this->boardState[$condition[0][0]][$condition[0][1]] == $player &&
				$this->boardState[$condition[0][0]][$condition[0][1]] == $this->boardState[$condition[1][0]][$condition[1][1]] &&
				$this->boardState[$condition[1][0]][$condition[1][1]] == $this->boardState[$condition[2][0]][$condition[2][1]]
			)
				return true;
		}
		return false;
	}

	/**
	 * Check is board index is taken before or not
	 *
	 * @param integer $index
	 * @return boolean
	 */
	public function isIndexTaken($index)
	{
		$row = $this->getRowByIndex($index);
		$column = $this->getColumnByIndex($index);

		if($this->boardState[$row][$column] != '')
			return true;
		return false;
	}

	private function getRowByIndex($index)
	{
		return (int) $index / 3;
	}

	private function getColumnByIndex($index)
	{
		return (int) $index % 3;
	}

	private function getBoardPositionIndex($row, $cell)
	{
		$start = 0;
	    if($row == 1)
		    $start = 3;
	    else if($row == 2)
		    $start = 6;

	    return $start + $cell;
	}
}