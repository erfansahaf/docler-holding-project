<?php

namespace App\Http\Handlers;

use App\Http\Contracts\BoardHandlerInterface;

class DefaultBoardHandler implements BoardHandlerInterface
{
	private $boardState = [];
	private $winningConditions = [
		[0, 1, 2],
		[3, 4, 5],
		[6, 7, 8],

		[0, 3, 6],
		[1, 4, 7],
		[2, 5, 8],

		[0, 4, 8],
		[2, 4, 6]
	];

	/**
	 *  Get current board state
	 *
	 * @return array
	 */
	public function getBoardState()
	{
		return $this->boardState;
	}

	/**
	 * Set board state
	 *
	 * @param array $boardState
	 * @return BoardHandlerInterface
	 */
	public function setBoardState($boardState)
	{
		$this->boardState = $boardState;
		return $this;
	}

	/**
	 * Getting default game board
	 *
	 * @return array
	 */
	public function getAnEmptyBoard()
	{
		return [
			'', '', '',
			'', '', '',
			'', '', ''
		];
	}

	/**
	 * Get those indexes which have not taken yet by any player
	 *
	 * @return array
	 */
	public function getBoardStateEmptyIndexes()
	{
		$emptyIndexes = [];
		foreach($this->boardState as $key => $item)
			if($item == '')
				$emptyIndexes[] = $key;

		return $emptyIndexes;
	}

	/**
	 * Get value of a specific board index
	 *
	 * @param integer $index
	 * @return string
	 */
	public function getBoardIndex($index)
	{
		return $this->boardState[$index];
	}

	/**
	 * Set value to a specific board index
	 *
	 * @param $index
	 * @param $player
	 * @return BoardHandlerInterface
	 */
	public function setBoardIndex($index, $player)
	{
		$this->boardState[$index] = $player;
		return $this;
 	}

	/**
	 * Checking the game state if it's Tie (Draw)
	 *
	 * @return boolean
	 */
	public function isTie()
	{
		$isBoardStateFilledOut = count($this->getBoardStateEmptyIndexes()) < 1 ;

		return $isBoardStateFilledOut;
	}

	/**
	 * Checking if the Player is winner
	 *
	 * @param string $player
	 * @return bool
	 */
	public function isPlayerWinner($player)
	{
		foreach ($this->winningConditions as $condition){
	        if(
	            $this->boardState[ $condition[0] ] == $player &&
		        $this->boardState[ $condition[0] ] == $this->boardState[ $condition[1] ] &&
		        $this->boardState[ $condition[1] ] == $this->boardState[ $condition[2] ]
	        )
		        return true;
        }
    return false;
	}

	/**
	 * Check is board index is taken before or not
	 *
	 * @param integer $index
	 * @return boolean
	 */
	public function isIndexTaken($index)
	{
		if($this->boardState[$index] != '')
			return true;
		return false;
	}
}