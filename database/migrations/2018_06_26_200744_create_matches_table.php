<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
	        $table->char('user_sign', '1');
	        $table->boolean('turn');
	        $table->json('board_state');
            $table->smallInteger('status')
	            ->unsigned()
	            ->default(get_const('match.status.created'))
                ->comment(implode_key_value(', ', get_const('match.status')));
            $table->string('board_handler')->default('default');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
