@extends('layouts.app')
@push('styles')
<style type="text/css">
    table {margin-top: 20px}
    td {
        width: 33%;
        padding: 0;
        height: 130px;
        vertical-align: middle !important;
        text-align: center;
        font-size: 45px;
        font-weight: bold;
    }
    td:hover {background-color: #f7f7f7;}
</style>
@endpush
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">New Match</div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8 offset-md-2">

                                <div class="row">
                                    <div class="col-md-12">
                                        <h3 class="">Turn:
                                            <span id="turn">Loading...</span>
                                        </h3>
                                    </div>

                                    <div class="col-md-12">
                                        <table class="table" border="1">
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </div>

                                    <div class="col-md-12">
                                        <h4 class="text-center">You play as: <span id="user" class="bold">loading...</span></h4>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    var USER_TURN = {{get_const("match.turn.user")}};
    var BOT_TURN  = {{get_const("match.turn.computer")}};
</script>
<script src="{{asset('js/game.js')}}"></script>
@endpush
