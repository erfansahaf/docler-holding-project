@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Ducler Holding</div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10">
                            <h3>Last matches</h3>
                        </div>
                        <div class="col-md-2">
                            <a href="{{route('new-match')}}" class="btn btn-primary pull-right">New match</a>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table">
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th class="text-center">Start time</th>
                                    <th class="text-center">Status</th>
                                </tr>
                                @foreach($matches as $match)
                                    <tr>
                                        <td class="text-center">{{$match->id}}</td>
                                        <td class="text-center">{{$match->created_at}}</td>
                                        <td class="text-center">
                                            @if($match->status == get_const('match.status.created'))
                                                <span class="badge badge-primary">Created</span>
                                            @elseif($match->status == get_const('match.status.not_finished'))
                                                <span class="badge badge-warning">Not Finished</span>
                                            @elseif($match->status == get_const('match.status.draw'))
                                                <span class="badge badge-secondary">Draw</span>
                                            @elseif($match->status == get_const('match.status.user_won'))
                                                <span class="badge badge-success">You Won</span>
                                            @elseif($match->status == get_const('match.status.computer_won'))
                                                <span class="badge badge-danger">You Lost</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
