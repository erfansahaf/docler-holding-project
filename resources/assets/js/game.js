/*
    Set Authorization header for every single HttpRequest (XHR)
 */
$.ajaxSetup({
    beforeSend: function (xhr)
    {
        const credential = localStorage.getItem('credential');
        xhr.setRequestHeader("Authorization","Basic " + credential);
    }
});

const SERVER         = "http://localhost:8000/";
const API_PREFIX     = "api/v1/";

const WIN_CONDITIONS = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],

    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],

    [0, 4, 8],
    [2, 4, 6]
];

const FIRST_COLOR    = 'text-primary';
const SECOND_COLOR   = 'text-warning';

let userSign         = 'X';
let botSign          = 'O';

let match            = null;
let turn             = null;
let boardState       = null;

let isGameFinished   = false;

$(document).ready(function () {
    startGame();

    $('td').click(function () {

        if(isGameFinished){
            alert('The game is over.');
            return;
        }

        let rowIndex  = $(this).parent().prevAll().length;
        let cellIndex = $(this).index();

        if(boardState[getBoardPositionIndex(rowIndex, cellIndex)] != ""){
            alert('This place is already taken. Select another one.');
            return;
        }

        $.ajax({
            url: SERVER + API_PREFIX + 'matches/' + match.id,
            type: 'POST',
            data: {index: getBoardPositionIndex(rowIndex, cellIndex)},
            success: function (response) {
                boardState = convertDoclerBoard(response.data.board_state);
                makeMove(rowIndex, cellIndex, getTurnPlayerSign(), getTurnClass());
                toggleTurn();
                setTimeout(() => {
                    drawDefaultBoard(boardState);
                    toggleTurn();
                }, 500);
            }
        });

    })
});

function startGame() {
    $.ajax({
        url: SERVER + API_PREFIX + 'matches',
        type: 'POST',
        success: function (response) {
            if(response.status != 201)
                return;
            match = response.data;
            boardState = convertDoclerBoard(match.board_state);
            turn = match.turn;
            if(match.user_sign == 'O'){
                userSign = 'O';
                botSign  = 'X';
            }
            updateTurnStatusSpan();
            updateUserSignSpan();
            drawDefaultBoard(boardState);
        }
    });
}

function makeMove(row, cell, playerSign, playerClass) {

    const selector = 'tr:eq('+row+') td:eq('+cell+')';

    $(selector).addClass(playerClass).text(playerSign);
    boardState[getBoardPositionIndex(row, cell)] = playerSign;

    if(isGameFinished)
        return;

    if(isPlayerWinningTheGame(boardState, botSign)){
        isGameFinished = true;
        setTimeout(() => {
            alert(`Player ` + botSign + ` won the game.`);
        }, 500);
    }
    else if(isPlayerWinningTheGame(boardState, userSign)){
        isGameFinished = true;
        setTimeout(() => {
            alert(`Player ` + userSign + ` won the game.`);
        }, 500);
    }
    else if(isTie(boardState)){
        isGameFinished = true;
        setTimeout(() => {
            alert(`No one won the game, it's Tie!`);
        }, 500);
    }
}

function drawDefaultBoard(board) {
    for(let i in board){
        let row = getRowByIndex(i);
        let cell = getCellByIndex(i);
        if(board[i] != '')
            makeMove(row, cell, board[i], board[i] == getTurnPlayerSign() ? getTurnClass() : getOppositeTurnClass());
    }
}

function isUserTurn() {
    return turn == USER_TURN;
}

function getTurnPlayerSign() {
    if(isUserTurn())
        return userSign;
    return botSign;
}

function getTurnClass() {
    if(isUserTurn())
        return FIRST_COLOR;
    return SECOND_COLOR;
}

function getOppositeTurnClass() {
    if(!isUserTurn())
        return FIRST_COLOR;
    return SECOND_COLOR;
}

function toggleTurn() {
    turn = isUserTurn() ? BOT_TURN : USER_TURN;
    updateTurnStatusSpan();
}

function updateTurnStatusSpan() {
    const $statusSelector = $('#turn');
    $statusSelector.removeClass();

    if(isGameFinished){
        $statusSelector.addClass('text-secondary').text('The game is over.');
        return;
    }

    $statusSelector.addClass(getTurnClass());
    $statusSelector.text(isUserTurn() ? 'You' : 'Computer');
}

function updateUserSignSpan() {
    const $selector = $("#user");
    $selector.text(userSign);
}

function getBoardPositionIndex(row, cell) {
    let start = 0;
    if(row == 1)
        start = 3;
    else if(row == 2)
        start = 6;
    return start + cell;
}

function getRowByIndex(indexNumber) {
    return parseInt(indexNumber / 3);
}

function getCellByIndex(indexNumber) {
    return indexNumber % 3;
}

function getBoardEmptyIndexes(board){
    let emptyIndexes = [];
    for(let i in board)
        if(board[i] == "")
            emptyIndexes.push(i);
    return emptyIndexes;
}

function isTie(board) {
    return getBoardEmptyIndexes(board).length == 0;
}

/*
    A function for converting DoclerHolding Board Format
 */
function convertDoclerBoard(board) {
    let newBoard = [];
    if(Array.isArray(board) && Array.isArray(board[0])){
        for(let i in board){
            for(let j in board[i]){
                newBoard.push(board[i][j]);
            }
        }
        return newBoard;
    }
    return board;
}

function isPlayerWinningTheGame(board, player) {
    for(let i in WIN_CONDITIONS){
        let square = WIN_CONDITIONS[i];
        if( board[ square[0] ] == player && board[ square[0] ] == board[ square[1] ] && board[ square[1] ] == board[ square[2] ]
        )
            return true;
    }
    return false;
}