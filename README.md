# Tic-Tac-Toe Game

This project is a PHP implementation of XO (Tic-Tac-Toe) game which is designed and coded to be expendable by adding new AI Algorithms. The first version which is the current version, has just a simple RandomSelection alogorithm which is suitable for "Easy" game level. Also, a `Minimax.php` file exists it `app\Http\Logic` directory which supposed to be a PHP implementation of Minimax algorithm  but I did not have enough time to write and test it.

## Installation
This project is written with PHP and Laravel 5.6 framework. So don't worry it is really easy to config and serve.

First, copy `.env.example` file with following command: 

`cp .env.example .env`

Then, open it and do your modifications and changes such as database name, database username or password. 

Second, run `composer update && npm install` command to download essential libraries such as Laravel and PHPUnit and NPM Packages.

Third, run `php artisan key:generate` to generate a encryption key. Laravel will set the generated key to related environment variable in .env file.

Finally, run `php artisan serve` to run project. After running this command, the project should be reachable by `http://localhost:8000` address.

## Board Handlers

There is two different Board handlers. The `default` and `docler` handlers that you can specify which one you want to use in your project in `.env` file. The `default` handler is my preferred type of creating and working with XO board and the `docler` one is the main format of working with board which I had to implement. You can learn more about these two by diving into the code! :)
 
 ## Tests
 
 My time was over and could not write test for the entire system but I tested DefaultBoardHandler class (Unit) and User Authentication (Integration) anyway. The tests can be found in `tests` directory.