<?php

Route::prefix('v1')->namespace('API\v1')->group(function (){
	/*
	 * Protect Game routes using Stateless Basic Authentication
	 *
	 * The request should have Authorization: Basic [credential] header
	 * Also, the [credential] is a base64-encoded string which contains username:password string
	 */
	Route::middleware('auth.basic.once')->group(function (){
		Route::post('matches', 'MatchesController@create');

		Route::post('matches/{id}', 'MatchesController@move');
	});
});