<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome')->name('welcome');
Auth::routes();

Route::middleware('auth')->group(function (){
	Route::get('/matches', 'MatchesController@index')->name('matches');
	Route::get('/new-match', 'MatchesController@new')->name('new-match');
});