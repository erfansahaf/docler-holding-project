<?php

return [
	'MATCH' => [
		'TURN'   => [
			'COMPUTER'      => 0,
			'USER'          => 1
		],
		'STATUS' => [
			'CREATED'      => 0,
			'NOT_FINISHED' => 1,

			'DRAW'         => 2,
			'USER_WON'     => 3,
			'COMPUTER_WON' => 4,
		]
	]
];